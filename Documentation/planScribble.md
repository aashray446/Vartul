# Vartul (Money Management App)
-- Everyone transaction , online or offline
-- Maintains Record Book

(This App Work is to make this easier and help them summarize it)

### DATA 
(That this app is going to deal)
1. Balance Sheet [Sender, Receiver, Amount , Category , Debit/Credit, ModeOfTransaction , DateAndTime]
2. People Data [Username, Password, number, email]

### USE CASES

(Bussiness Logic)
1. Can record multiple balance sheet which records their transactions. ( #TRANSACTION )
2. Help Know them the way they are spending  ( #SEE )
3. Can record balance sheet of transactions done between people (Loan Book) -- Interconnected between two account holders #(MAINTAIN)

### UI DECIDED

(RELATED TO PEOPLE DATA)
[PROFILE PAGE, PEOPLES PAGE]

#### Some Ideas related to how details can be managed
 (About Sheet)
1. A sheet can have one or multiple people 
2. A sheet will have it's own customizable categories
3. A sheet can be shareable betweeen people 

### Figma Brainstorm File
https://www.figma.com/file/3OMju3UFAiK3kwoJyV11Hh/Untitled?node-id=0%3A1